
Sample1: ConnectMeta.cpp HEpoll.cpp Sample1.cpp Socket.cpp
	g++ -Wall -g $^ -o $@ -lpthread -std=c++11

clean:
	rm -f *.o hep

server: ConnectMeta.cpp HEpoll.cpp ServerTest.cpp Socket.cpp
	g++ -Wall -g $^ -o $@ -lpthread -std=c++11

client: ConnectMeta.cpp HEpoll.cpp ClientTest.cpp Socket.cpp
	g++ -Wall -g $^ -o $@ -lpthread -std=c++11
