#include <cstdio>
#include <iostream>
#include "AliasRef.h"

using namespace std;
#define Print(x) cout << "Var " #x "=" << x << endl;


int main(int, char**)
{
    AliasRef<int> arf;

    arf.addAlias("n1", 1234);
    arf.addAlias("n2", 1234);
    arf.addAlias("t1", 678);
    arf.addAlias("t2", 678);
    arf.addAlias("t12", 678);

    auto v1 = arf.find("na2");
    Print(v1);

    arf.delAlias("kkk");
    arf.delAlias("t1");
    
    auto v2 = arf.find("n2");
    Print(v2);


    return 0;
}
