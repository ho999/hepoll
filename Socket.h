/*-------------------------------------------------------------------------
FileName     : Socket.h
Description  : 套接字的操作封装
remark       : 
Modification :
--------------------------------------------------------------------------
   1、Date  2019-11-05       create     hejl 
-------------------------------------------------------------------------*/
#ifndef _SOCKET_H_
#define _SOCKET_H_
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>

#define sock_nonblock(s)  fcntl(s, F_SETFL, fcntl(s, F_GETFL) | O_NONBLOCK)
#define sock_block(s)     fcntl(s, F_SETFL, fcntl(s, F_GETFL) & ~O_NONBLOCK)
#define sock4_ntop(str, addr) char str[16]={0}; \
    inet_ntop(AF_INET, (void *)&addr.sin_addr, str, 16);
#define sock6_ntop(str, addr) char str[40]={0}; \
    inet_ntop(AF_INET6, (void *)&addr.sin6_addr, str, 40);
#define sock4_port(addr) ntohs(addr.sin_port)
#define sock6_port(addr) ntohs(addr.sin6_port)

#define LISTEN_BACKLOG 128 // listen()等待队列大小
using std::string;

enum ErrSock
{
    ERRSOCK_UNKNOW = -4,
    ERRSOCK_PARAM = -3, // 参数错误
    ERRSOCK_AGAIN = -2, // noblock socket暂时缓冲区中无数据
    ERRSOCK_FAIL = -1,  // socket检测到出错
    ERRSOCK_CLOSE = 0,  // 收到EOF

    ERRSOCK_CONNECT = -5, // connect fail
};

class Socket
{
public:
    // 创建监听套接字
    static int create_fd(const char* ip, int port, bool udp = false, bool v6 = false); 
    static int connect(int& fd, const char* host, int port, int timout_sec, bool noblock);
    static int connect_noblock(int& fd, const char* host, int port);

    static string sock_name(int fd, bool hasport = false, bool v6 = false);
    static string peer_name(int fd, bool hasport = false, bool v6 = false);
    static int geterrno(int fd);

    static int read(int fd, char* buff, unsigned& begpos, unsigned bufflen);
    static int recv(int fd, char* buff, unsigned& begpos, unsigned bufflen);
    static int send(int fd, char* buff, unsigned& begpos, unsigned bufflen);

    static int setRcvTimeOut(int fd, int sec);
    static int setSndTimeOut(int fd, int sec);

    static int setSndBuffSize(int fd, int bytes);
    static int setRcvBuffSize(int fd, int bytes);

    static int setKeepAlive(int fd, int keepAliveSec, int testIntvalSec, int probeCount);

private:
    template<bool TPeer> static string _getName_(int fd, bool hasport, bool v6);
    static int addr_type(const char* addr);
    static int host2Ip(string& addrHost);
};

#endif

